@extends('layouts.master')

@section('content')
<div class="card">
  <div class="card-header">
    <h3 class="card-title">LIST DATA PERTANYAAN</h3>
  </div>
  <div class="card-body">
    @if(session('success'))
      <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">x</button>
        {{ session('success') }}
      </div>
    @endif
    <table id="example1" class="table table-bordered table-striped">
      <a href="/pertanyaan/create" class="btn btn-primary mb-3">Tambah Data Pertanyaan</a>
      <thead class="thead-light">
        <tr>
          <th style="width:10px;">#</th>
          <th>Judul</th>
          <th>Isi</th>
          <th>Tanggal Dibuat</th>
          <th style="width:40px;">Actions</th>
        </tr>
      </thead>
      <tbody>
        @forelse($pertanyaan as $key => $data)
        <tr>
          <td>{{ $key+1 }}</td>
          <td>{{ $data->judul }}</td>
          <td>{{ $data->isi }}</td>
          <td>{{ $data->tanggal_dibuat }}</td>
          <td style="display:flex;">
            <a href="/pertanyaan/{{$data->id}}" class="btn btn-success btn-sm mr-1" style="width:100px;">Lihat Data</a>
            <a href="/pertanyaan/{{$data->id}}/edit" class="btn btn-warning btn-sm mr-1" style="width:100px;">Ubah Data</a>
            <form action="/pertanyaan/{{$data->id}}" method="POST">
              @csrf
              @method('DELETE')
              <input type="submit" class="btn btn-danger btn-sm mr-1 " style="width:100px;" value="Hapus Data">
            </form>   
          </td>
        </tr>
        @empty
        <tr>
          <td colspan="5" align="center">Tidak Ada Post</td>
        </tr>
        @endforelse
      </tbody>
    </table>
  </div>
</div>
@endsection