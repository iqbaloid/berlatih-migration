@extends('layouts.master')

@section('content')

<div class="container-fluid mt-2">
    <div class="row">
        <!-- left column -->
        <div class="col-md">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Ubah Data Pertanyaan {{ $pertanyaan->id }}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/pertanyaan/{{ $pertanyaan->id }}" method="POST">
                @csrf
                @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Judul Pertanyaan</label>
                    <input type="text" class="form-control" name="judul" id="judul" value="{{ old('judul', $pertanyaan->judul) }}" required>
                    @error('judul')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>

                  <div class="form-group">
                    <label for="exampleInputEmail1">Isi Pertanyaan</label>
                    <textarea class="textarea" placeholder="Masukkan Isi" name="isi" id="isi" required
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ old('isi', $pertanyaan->judul) }}</textarea>
                    @error('isi')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>

                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                <button type="submit" class="btn btn-primary">Ubah</button>
                <a href="/pertanyaan" class="btn btn-secondary"> Kembali</a>
                </div>
              </form>
            </div>
            <!-- /.card -->
        </div>
    </div>
</div>

@endsection