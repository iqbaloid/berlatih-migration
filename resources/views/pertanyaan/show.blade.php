@extends('layouts.master')

@section('content')

<div class="container-fluid mt-2">
    <div class="row">
        <!-- left column -->
        <div class="col-md">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Detail Data Pertanyaan {{ $pertanyaan->id }}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Judul Pertanyaan</label>
                    <input type="text" class="form-control" name="judul" id="judul" value="{{ $pertanyaan->judul }}" disabled>
                  </div>

                  <div class="form-group">
                    <label for="exampleInputEmail1">Isi Pertanyaan</label>
                    <textarea class="textarea" placeholder="Masukkan Isi" name="isi" id="isi" disabled
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ $pertanyaan->isi }}</textarea>
                  </div>

                  <div class="form-group">
                    <label for="exampleInputEmail1">Tanggal Dibuat</label>
                    <input type="text" class="form-control" name="tgl" id="tgl" value="{{ $pertanyaan->tanggal_dibuat }}" disabled>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                <a href="/pertanyaan" class="btn btn-secondary"> Kembali</a>
                </div>
              </form>
            </div>
            <!-- /.card -->
        </div>
    </div>
</div>

@endsection